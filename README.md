Chris Chiarenza is the Founder and President of DealHouse. He began his real estate career in 2009 and since then he has purchased, fixed, and flipped hundreds of homes right on Long Island.

Website : https://dealhouse.com/